function network = backward_propagate_error(network,expected)
    for i = size(network.weight,2):-1:1
       layer = network.weight{i};
       errors = [];
       if i ~= size(network.weight,2)
           for j = 1:size(network.weight{i},1)
               error = 0;
               for neuron = 1:size(network.weight{i + 1},1)
                   error = error + (network.weight{i+1}{neuron,j} * network.delta(i+1,neuron) );
               end
               errors = [errors error];
           end
       else
           for j = 1:size(network.weight{i},1)
               error_value = expected(j) - network.output(i,j);
               errors = [errors  error_value];
           end
       end
       for j = 1:size(network.weight{i},1)
           network.delta(i,j) = errors(j) * transfer_derivative(network.output(i,j));
       end
    end
end