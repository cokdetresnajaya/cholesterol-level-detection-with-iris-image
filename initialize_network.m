function network = initialize_network(neuron_inputs,neuron_hiddens,neuron_outputs)
   for layer = 1:2
       if(layer == 1)
           for i = 1:neuron_hiddens
               for j = 1:neuron_inputs + 1
                   hidden_weight{i,j} = rand;
               end
           end
           network.weight{layer} = hidden_weight;
       else
           for i = 1:neuron_outputs
               for j = 1:neuron_hiddens+1
                   output_weight{i,j} = rand;
               end
           end
           network.weight{layer} = output_weight;
       end
   end
end

