function activation = activate(weights,inputs)
    activation = 0;
    for i = 1:size(weights,2)-1
        activation = activation + (weights{i} * inputs(i));
    end
    activation = activation + weights{size(weights,2)};
end

