function [center_iris,center_pupil,output] = iris_segmentation(image,min_radius,max_radius)
    original_image = image;
    
    %menghilangkan refleksi cahaya pada mata
    image = imcomplement(imfill(imcomplement(image), 'holes'));
    
    %Thresholding
    [radius,c] = size(image); %mengambil jumlah baris dan kolom piksel
    [x,y] = find(image < 0.5); %memilih piksel dengan nilai keabuan < 0.5 (nilai threshold)
    
    for k = 1:size(x, 1)
        if (x(k) > min_radius) & (y(k) > min_radius) & (x(k) <= (radius - min_radius)) & (y(k)<(c - min_radius))
            % Mengecek ketetanggaan dari piksel saat ini
            V = image((x(k) - 1):(x(k) + 1), (y(k) - 1):(y(k) + 1));
            min_pixel = min(min(V));
            if image(x(k), y(k)) ~= min_pixel
                x(k) = 0;
                y(k) = 0;
            end
        end
    end
    
    %mengambil indeks data dari lokasi kolom dan baris yang tidak memenuhi persyaratan
    k = find((x <= min_radius) | (y <= min_radius) | (x > (radius - min_radius)) | (y > (c - min_radius)));
    x(k) = [];
    y(k) = [];
    n = size(x, 1); %total piksel setelah eliminasi
    
    %menghitung nilai partial derivative dari setiap piksel
    for k = 1:n
        [b, radius, blur] = partial_derivative(image, [x(k), y(k)], min_radius, max_radius, 'iris'); %coarse search
        max_b(x(k), y(k)) = b;
    end
    [x,y] = find(max_b == max(max(max_b)));
    
    % Mencari titik pusat iris dan radiusnya
    center_iris = find_center(image, min_radius, max_radius, x, y, 'iris');
    
    % Mencari titik pusat pupil dan radiusnya
    center_pupil = find_center(image, round(0.1 * radius), round(0.8 * radius), center_iris(1), center_iris(2), 'pupil');
    
    % Menggambar lingkaran untuk iris dan pupil
    output = draw_circle(original_image,[center_iris(1),center_iris(2)],center_iris(3),600);
    output = draw_circle(output,[center_pupil(1),center_pupil(2)],center_pupil(3),600);
end