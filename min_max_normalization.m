function [normalizeValue] = min_max_normalization(value,newMinValue,newMaxValue,minValue,maxValue)
    %minValue = min(value);
    %maxValue = max(value);
    range = maxValue - minValue;
    value = value - minValue;
    value = value / range;
    newRange = newMaxValue - newMinValue;
    value = (value * newRange) + newMinValue;
    normalizeValue = value;
end

