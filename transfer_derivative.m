function output = transfer_derivative(output)
    output = output * (1 - output);
end

