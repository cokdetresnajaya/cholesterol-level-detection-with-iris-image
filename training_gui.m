function varargout = training_gui(varargin)
% TRAINING_GUI MATLAB code for training_gui.fig
%      TRAINING_GUI, by itself, creates a new TRAINING_GUI or raises the existing
%      singleton*.
%
%      H = TRAINING_GUI returns the handle to a new TRAINING_GUI or the handle to
%      the existing singleton*.
%
%      TRAINING_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TRAINING_GUI.M with the given input arguments.
%
%      TRAINING_GUI('Property','Value',...) creates a new TRAINING_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before training_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to training_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help training_gui

% Last Modified by GUIDE v2.5 18-Dec-2020 15:36:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @training_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @training_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before training_gui is made visible.
function training_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to training_gui (see VARARGIN)

% Choose default command line output for training_gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes training_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = training_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dirPath = uigetdir();
set(handles.figure1, 'pointer', 'watch');
drawnow;
abnormalData = dir([dirPath '/*.jpg']);
numberOfAbnormalImage = numel(abnormalData);
for i = 1:numberOfAbnormalImage
    currentFileName = abnormalData(i).name;
    currentImage = imread(fullfile(dirPath,currentFileName));
    images{i} = currentImage;
    grayImage = im2double(rgb2gray(images{i}));
    [center_iris,center_pupil,output] = iris_segmentation(grayImage,25,100);
    normImage = normalize_iris(grayImage,center_pupil,center_iris,40,240);
    roiImage = imcrop(normImage,[0 size(normImage,1)-25 size(normImage,2) size(normImage,1)]);
    glcms = graycomatrix(roiImage);
    [feature] = GLCM_Features1(glcms,0);
    data(i,1:8) = {currentFileName feature.contr feature.corrm feature.energ feature.homom feature.entro 'Abnormal' ''}; 
end
set(handles.uitable2,'data',data);
set(handles.figure1, 'pointer', 'arrow');
set(handles.pushbutton2,'enable','on');
set(handles.pushbutton1,'enable','off');


function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
dirPath = uigetdir();
set(handles.figure1, 'pointer', 'watch');
drawnow;
normalData = dir([dirPath '/*.jpg']);
numberOfNormalImage = numel(normalData);
data = get(handles.uitable2,'data');
j = 1;
for i = size(data) + 1 : size(data) + numberOfNormalImage
    currentFileName = normalData(j).name;
    currentImage = imread(fullfile(dirPath,currentFileName));
    images{j} = currentImage;
    grayImage = im2double(rgb2gray(images{j}));
    [center_iris,center_pupil,output] = iris_segmentation(grayImage,25,100);
    normImage = normalize_iris(grayImage,center_pupil,center_iris,40,240);
    roiImage = imcrop(normImage,[0 size(normImage,1)-25 size(normImage,2) size(normImage,1)]);
    glcms = graycomatrix(roiImage);
    [feature] = GLCM_Features1(glcms,0);
    data(i,1:8) = {currentFileName feature.contr feature.corrm feature.energ feature.homom feature.entro 'Normal' ''};
    j = j + 1;
end
set(handles.uitable2,'data',data);
set(handles.figure1, 'pointer', 'arrow');
set(handles.pushbutton2,'enable','off');
set(handles.pushbutton3,'enable','on');


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
hidden_neuron = get(handles.edit1,'String');
l_rate = get(handles.edit2,'String');
n_epoch = get(handles.edit3,'String');
min_error = get(handles.edit4,'String');
n_class = 2;

if isempty(hidden_neuron)|isempty(l_rate)|isempty(n_epoch)|isempty(min_error)
    set(handles.figure1, 'pointer', 'watch');
    drawnow;
    f = warndlg('Seluruh parameter harus diisi!');
    set(handles.figure1, 'pointer', 'arrow');
else
    hidden_neuron = str2num(hidden_neuron);
    l_rate = str2num(l_rate);
    n_epoch = str2num(n_epoch);
    min_error = str2num(min_error);
    
    if isempty(hidden_neuron)|isempty(l_rate)|isempty(n_epoch)|isempty(min_error)
        set(handles.figure1, 'pointer', 'watch');
        drawnow;
        f = warndlg('Terdapat field yang tidak berformat numerik!');
        set(handles.figure1, 'pointer', 'arrow');
    else
        if hidden_neuron ~= round(hidden_neuron) | n_epoch ~= round(n_epoch)
            set(handles.figure1, 'pointer', 'watch');
            drawnow;
            f = warndlg('Field jumlah hidden neuron dan jumlah epoch harus integer!');
            set(handles.figure1, 'pointer', 'arrow');
        else
            set(handles.figure1, 'pointer', 'watch');
            drawnow;
            dataTable = get(handles.uitable2,'data');
            
            %konversi dari cell ke matriks
            for i = 1:size(dataTable,1)
                for j = 2:size(dataTable,2)-1
                   k = j - 1;
                   if j == 7
                       if strcmp('Abnormal',dataTable{i,j})
                           data(i,k) = 1;
                       else
                           data(i,k) = 0;
                       end
                   else
                       data(i,k) = dataTable{i,j};
                   end
                end
                entropiData(i) = dataTable{i,6};
            end
            minValue = min(entropiData);
            maxValue = max(entropiData);
            newEntropi = min_max_normalization(entropiData,0,1,minValue,maxValue);
            handles.minEntropi = minValue;
            handles.maxEntropi = maxValue;
            for i = 1 : size(data,1)  %v salah
                data(i,5) = newEntropi(i);
            end
            
            %inisialisi jaringan
            network = initialize_network(size(data,2)-1,hidden_neuron,n_class);
            
            %proses data training
            network = train_network(network,data,l_rate,n_epoch,n_class,min_error);
            figure;
            plot(network.epoch,network.mse);
            true_prediction = 0;
            false_prediction = 0;
            %mencari akurasi training
            for i=1:size(data,1)
                prediction = predict(network,data(i,:));
                expected = data(i,size(data,2));
                if prediction == 1
                    dataTable{i,size(dataTable,2)} = 'Abnormal';
                elseif prediction == 0
                    dataTable{i,size(dataTable,2)} = 'Normal'; 
                end
                
                if prediction == data(i,size(data,2))
                    true_prediction = true_prediction + 1;
                else
                    false_prediction = false_prediction + 1;
                end
                
                text = sprintf('>Expected=%d, Got=%d',expected,prediction);
                display(text);
            end
            
            accuration = true_prediction / (size(data,1)) * 100;
            set(handles.uitable2,'data',dataTable);
            set(handles.edit6,'string',num2str(size(data,1)));
            set(handles.edit7,'string',num2str(true_prediction));
            set(handles.edit8,'string',num2str(false_prediction));
            accuration = strcat(num2str(accuration),' %');
            set(handles.edit9,'string',num2str(accuration));
            f = msgbox('Proses pelatihan data telah selesai.');
            set(handles.figure1, 'pointer', 'arrow');
            handles.weight = network.weight;
            %set(handles.pushbutton3,'enable','off');
            set(handles.pushbutton4,'enable','on');
        end
    end
end
guidata(hObject, handles);



% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
prompt = {'Masukkan nama file :'};
dlgtitle = 'Nama File';
width = 50;
height = 1;
num_lines = [height,width];
set(handles.figure1, 'pointer', 'watch');
drawnow;
answer = inputdlg(prompt,dlgtitle,num_lines);
set(handles.figure1, 'pointer', 'arrow');


if ~isempty(answer)
    filename = string(answer);
    if filename == ""
        set(handles.figure1, 'pointer', 'watch');
        drawnow;
        f = warndlg('Nama file harus diisi.','Nama File');
        set(handles.figure1, 'pointer', 'arrow');
    else
        folder = strcat(pwd,'\network_data');
        if ~exist(folder, 'dir')
            mkdir(folder);
        end
        filename = strcat(filename,'.xls');
        fullFileName = strcat(folder,'\',filename);
        
        if exist(fullFileName,'file')
            set(handles.figure1, 'pointer', 'watch');
            drawnow;
            f = warndlg('Nama file sudah ada.','Nama File');
            set(handles.figure1, 'pointer', 'arrow');
        else
            hidden_neuron = get(handles.edit1,'String');
            architecture = [5, str2num(hidden_neuron), 2];
            entropiValue = [handles.minEntropi handles.maxEntropi];
            %menyimpan arsitektur jaringan
            xlswrite(fullFileName,architecture,1,'A1:C1');
            %menyimpan batas bawah dan atas data entropi untuk normaliasi
            xlswrite(fullFileName,entropiValue,1,'E1:F1');
            for i = 1:size(handles.weight,2)
                for j = 1:size(handles.weight{i},1)
                    for k = 1:size(handles.weight{i},2)
                        if i == size(handles.weight,2)
                            weight_out(j,k) = handles.weight{i}{j,k};
                        else
                            weight_hidden(j,k) = handles.weight{i}{j,k};
                        end
                    end
                end
            end
            hidden_row = 3;
            hidden_col = 'A';

            %menyimpan nilai bobot yang menuju ke hidden layer
            xlswrite(fullFileName,weight_hidden,1,'A3');

            out_row = (hidden_row + architecture(2)) + 1;
            out_col = hidden_col;
            out_start_cell = strcat(out_col,num2str(out_row));

            %menyimpan nilai bobot yang menuju ke output layer
            xlswrite(fullFileName,weight_out,1,out_start_cell);

            f = msgbox('Bobot dan jaringan telah disimpan.');
        end 
    end
end 
set(handles.pushbutton4,'enable','off');


function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit8_Callback(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit8 as text
%        str2double(get(hObject,'String')) returns contents of edit8 as a double


% --- Executes during object creation, after setting all properties.
function edit8_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton1,'enable','on');
set(handles.pushbutton2,'enable','off');
set(handles.pushbutton3,'enable','off');
set(handles.pushbutton4,'enable','off');
set(handles.edit1,'string','');
set(handles.edit2,'string','');
set(handles.edit3,'string','');
set(handles.edit4,'string','');
set(handles.edit6,'string','');
set(handles.edit7,'string','');
set(handles.edit8,'string','');
set(handles.edit9,'string','');
set(handles.uitable2, 'Data', {});


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close;
run('main_gui.m');
