function prediction = predict(network,row)
    [network,outputs] = forward_propagate(network,row);
    prediction = find(outputs==max(outputs))-1;
end

