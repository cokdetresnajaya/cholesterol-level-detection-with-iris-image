function varargout = main_gui(varargin)
% MAIN_GUI MATLAB code for main_gui.fig
%      MAIN_GUI, by itself, creates a new MAIN_GUI or raises the existing
%      singleton*.
%
%      H = MAIN_GUI returns the handle to a new MAIN_GUI or the handle to
%      the existing singleton*.
%
%      MAIN_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in MAIN_GUI.M with the given input arguments.
%
%      MAIN_GUI('Property','Value',...) creates a new MAIN_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before main_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to main_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help main_gui

% Last Modified by GUIDE v2.5 21-Dec-2020 13:10:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @main_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @main_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before main_gui is made visible.
function main_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to main_gui (see VARARGIN)

% Choose default command line output for main_gui
handles.output = hObject;

set(handles.axes1,'XColor', 'none','YColor','none');
set(handles.axes2,'XColor', 'none','YColor','none');
set(handles.axes3,'XColor', 'none','YColor','none');
set(handles.axes4,'XColor', 'none','YColor','none');
set(handles.pushbutton2,'enable','off');
set(handles.pushbutton3,'enable','off');
set(handles.pushbutton4,'enable','off');
set(handles.pushbutton5,'enable','off');
set(handles.pushbutton7,'enable','off');
handles.status = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes main_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = main_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('*.jpg');
set(handles.figure1, 'pointer', 'watch');
drawnow;
if isequal(file,0)
    disp('User selected canceled');
else
    handles.imageFullPath = fullfile(path,file);
    axes(handles.axes1);
    imshow(handles.imageFullPath);
    handles.status = 1;
    
    %format gui setelah berhasil input gambar
    set(handles.pushbutton2,'enable','on');
    set(handles.pushbutton3,'enable','off');
    set(handles.pushbutton4,'enable','off');
    set(handles.pushbutton5,'enable','off');
    set(handles.pushbutton7,'enable','off');
    cla(handles.axes2,'reset');
    cla(handles.axes3,'reset');
    cla(handles.axes4,'reset');
    set(handles.axes2,'XColor', 'none','YColor','none');
    set(handles.axes3,'XColor', 'none','YColor','none');
    set(handles.axes4,'XColor', 'none','YColor','none');
    set(handles.edit1,'String','');
    set(handles.edit2,'String','');
    set(handles.edit3,'String','');
    set(handles.edit4,'String','');
    set(handles.edit5,'String','');
    set(handles.edit6,'String','');
    
    guidata(hObject,handles);
end
set(handles.figure1, 'pointer', 'arrow');

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1, 'pointer', 'watch');
drawnow;
handles.grayScaleImage = im2double(rgb2gray(imread(handles.imageFullPath)));
axes(handles.axes2);
imshow(handles.grayScaleImage);
handles.status = 2;

%format gui setelah grayscaling
set(handles.pushbutton3,'enable','on');
set(handles.pushbutton4,'enable','off');
set(handles.pushbutton5,'enable','off');
set(handles.pushbutton7,'enable','off');
cla(handles.axes3,'reset');
cla(handles.axes4,'reset');
set(handles.axes3,'XColor', 'none','YColor','none');
set(handles.axes4,'XColor', 'none','YColor','none');
set(handles.edit1,'String','');
set(handles.edit2,'String','');
set(handles.edit3,'String','');
set(handles.edit4,'String','');
set(handles.edit5,'String','');
set(handles.edit6,'String','');

guidata(hObject, handles);
set(handles.figure1, 'pointer', 'arrow');


% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1, 'pointer', 'watch');
drawnow;

[center_iris,center_pupil,output] = iris_segmentation(handles.grayScaleImage,25,100);
handles.ciTest = center_iris;
handles.cpTest = center_pupil;
axes(handles.axes3);
imshow(output);
handles.status = 3;

%format gui setelah segmentasi iris
set(handles.pushbutton4,'enable','on');
set(handles.pushbutton5,'enable','off');
set(handles.pushbutton7,'enable','off');
cla(handles.axes4,'reset');
set(handles.axes4,'XColor', 'none','YColor','none');
set(handles.edit1,'String','');
set(handles.edit2,'String','');
set(handles.edit3,'String','');
set(handles.edit4,'String','');
set(handles.edit5,'String','');
set(handles.edit6,'String','');

guidata(hObject, handles);
set(handles.figure1, 'pointer', 'arrow');


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1, 'pointer', 'watch');
drawnow;

handles.normImage = normalize_iris(handles.grayScaleImage,handles.cpTest,handles.ciTest,40,240);
axes(handles.axes4);
imshow(handles.normImage);
handles.status = 4;

set(handles.pushbutton5,'enable','on');
set(handles.pushbutton7,'enable','off');
set(handles.edit1,'String','');
set(handles.edit2,'String','');
set(handles.edit3,'String','');
set(handles.edit4,'String','');
set(handles.edit5,'String','');
set(handles.edit6,'String','');

guidata(hObject, handles);
set(handles.figure1, 'pointer', 'arrow');



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.figure1, 'pointer', 'watch');
drawnow;

handles.roiImage = imcrop(handles.normImage,[0 size(handles.normImage,1)-25 size(handles.normImage,2) size(handles.normImage,1)]);
glcms = graycomatrix(handles.roiImage);
[featureTest] = GLCM_Features1(glcms,0);
set(handles.edit1,'string',featureTest.contr);
set(handles.edit2,'string',featureTest.corrm);
set(handles.edit3,'string',featureTest.energ);
set(handles.edit4,'string',featureTest.homom);
set(handles.edit5,'string',featureTest.entro);
inputs = [featureTest.contr featureTest.corrm featureTest.energ featureTest.homom featureTest.entro];
handles.featureTest = inputs;

set(handles.pushbutton7,'enable','on');
guidata(hObject, handles);
set(handles.figure1, 'pointer', 'arrow');


% --- Executes on button press in pushbutton6.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close;
run('training_gui.m');


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[file,path] = uigetfile('*.xls');
set(handles.figure1, 'pointer', 'watch');
drawnow;

if isequal(file,0)
    display('select weight data canceled');
else
    fullFileName = fullfile(path,file);
    sheet = 1;
    architecture_range = 'A1:C1';
    architecture = xlsread(fullFileName,sheet,architecture_range);
    
    hidden_start_col = 1;
    hidden_start_row = 3;
    hidden_end_col = hidden_start_col + 5;
    hidden_end_row = hidden_start_row + (architecture(2) - 1);
    
    column_name = xlsColNum2Str(hidden_start_col);
    hidden_start_range = strcat(column_name{1},string(hidden_start_row));
    column_name = xlsColNum2Str(hidden_end_col);
    hidden_end_range = strcat(column_name{1},string(hidden_end_row));
    
    network_weight = xlsread(fullFileName,sheet,strcat(hidden_start_range,':',hidden_end_range));
    for i = 1:size(network_weight,1)
        for j = 1:size(network_weight,2)
            network.weight{1}{i,j} = network_weight(i,j);
        end
    end
    
    output_start_row = hidden_end_row + 2;
    output_end_row = output_start_row + (architecture(3) - 1);
    output_end_col = hidden_start_col + architecture(2);
    
    column_name = xlsColNum2Str(hidden_start_col);
    output_start_range = strcat(column_name{1},string(output_start_row));
    column_name = xlsColNum2Str(output_end_col);
    output_end_range = strcat(column_name{1},string(output_end_row));
    
    network_weight = xlsread(fullFileName,sheet,strcat(output_start_range,':',output_end_range));
    
    for i = 1:size(network_weight,1)
        for j = 1:size(network_weight,2)
            network.weight{2}{i,j} = network_weight(i,j);
        end
    end
    %mengambil nilai min dan max untuk normalisasi data
    boundary = xlsread(fullFileName,sheet,'E1:F1');
    inputs = handles.featureTest;
    newEntropi = min_max_normalization(inputs(5),0,1,boundary(1),boundary(2));
    inputs(5) = newEntropi;
    
    prediction = predict(network,inputs);
    if(prediction == 1)
        set(handles.edit6,'String','Kolesterol Tinggi');
    else
        set(handles.edit6,'String','Kolesterol Normal');
    end
    
end
set(handles.figure1, 'pointer', 'arrow');



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton2,'enable','off');
set(handles.pushbutton3,'enable','off');
set(handles.pushbutton4,'enable','off');
set(handles.pushbutton5,'enable','off');
set(handles.pushbutton7,'enable','off');

cla(handles.axes1,'reset');
cla(handles.axes2,'reset');
cla(handles.axes3,'reset');
cla(handles.axes4,'reset');
set(handles.axes1,'XColor', 'none','YColor','none');
set(handles.axes2,'XColor', 'none','YColor','none');
set(handles.axes3,'XColor', 'none','YColor','none');
set(handles.axes4,'XColor', 'none','YColor','none');

set(handles.edit1,'String','');
set(handles.edit2,'String','');
set(handles.edit3,'String','');
set(handles.edit4,'String','');
set(handles.edit5,'String','');
set(handles.edit6,'String','');