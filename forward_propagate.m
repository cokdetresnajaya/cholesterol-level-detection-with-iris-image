function [network,inputs] = forward_propagate(network,row)
    inputs = row;
    for layer = 1:size(network.weight,2)
        new_inputs = [];
        for neuron = 1:size(network.weight{layer},1)
            activation = activate(network.weight{layer}(neuron,:),inputs);
            network.output(layer,neuron) = transfer(activation);
            new_inputs = [new_inputs network.output(layer,neuron)];
        end
        inputs = new_inputs;
    end
end

