function network = update_weights(network,row,l_rate)
    for i = 1:size(network.weight,2)
        inputs = row(1:size(row,2)-1);
        if i ~= 1
            for j = 1:size(network.weight{i - 1},1)
                inputs(j) = network.output(i - 1,j);
            end
        end
        for neuron = 1:size(network.weight{i},1)
            for j = 1:size(inputs,2)
                network.weight{i}{neuron,j} = network.weight{i}{neuron,j} + (l_rate * network.delta(i,neuron)* inputs(j));
            end
            network.weight{i}{neuron,size(network.weight{i}{neuron},2)} = network.weight{i}{neuron,size(network.weight{i}{neuron},2)} + (l_rate * network.delta(i,neuron));
        end
    end
end

