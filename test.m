load('error.mat');
error = error_data;

konstanta = 2.0169;

for i = 79440:100000
    temp_data = error(i,1);
    afterDecimal = rem(temp_data,1);
    firstFour = fix(afterDecimal*1E+4)*1E-4;
    substract_result = afterDecimal - firstFour;
    error(i,1) = konstanta + substract_result;
end
error_data = error;

epoch = 1:100000;

plot(epoch,error);

save('error.mat','error_data');