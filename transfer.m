function output = transfer(activation)
    output = 1.0 / (1.0 + exp(-activation));
end

