function network = train_network(network,train,l_rate,n_epoch,n_outputs,goal)
    for i=1:n_epoch
        sum_error = 0;
        
        for j = 1:size(train,1)
            [network,outputs] = forward_propagate(network,train(j,:));
            for k=1:n_outputs
                expected(k) = 0;
            end
            expected(train(j,size(train,2)) + 1) = 1;
            temp = [];
            for k=1:size(expected,2)
                temp = [temp power(expected(k) - outputs(k),2)];
            end
            sum_error = sum_error + sum(temp); 
            mse = sum_error/n_outputs;
            network = backward_propagate_error(network,expected);
            network = update_weights(network,train(j,:),l_rate);
        end
        text = sprintf('>epoch=%d, l_rate=%f, error=%f',i,l_rate,mse);
        display(text);
        network.mse(i) = mse;
        %error_value(i,1) = mse;
        network.epoch(i) = i;
        if(mse <= goal)
            break;
        end
    end
    %error_data = error_value;
    %save('error','error_data');
end

